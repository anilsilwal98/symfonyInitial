<?php

namespace ProjectName\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

    public function indexAction()
    {
        return $this->render('ProjectNameFrontendBundle::__layout.html.twig');
    }
}
